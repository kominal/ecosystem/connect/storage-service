import Service from '@kominal/service-util/helper/service';
import get from './routes/get';
import save from './routes/save';
import gridfs from './routes/gridfs';
import remove from './routes/remove';

const service = new Service({
	id: 'storage-service',
	name: 'Storage Service',
	description: 'Stores encrypted data.',
	jsonLimit: '16mb',
	routes: [get, save, gridfs, remove],
	database: true,
});
service.start();

export default service;
