# Storage Service

The storage service is responsible for storing encrypted data. The data to be stored is encrypted on the client and sent via HTTPS to the service.
The service will persist the data in a MongoDB (running on multiple availabilty zones using a replica set).

## Documentation

Production: https://connect.kominal.com/storage-service/api-docs
Test: https://connect-test.kominal.com/storage-service/api-docs
