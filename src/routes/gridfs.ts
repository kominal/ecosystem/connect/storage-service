import { verifyParameter } from '@kominal/service-util/helper/util';
import Router from '@kominal/service-util/helper/router';
import Busboy from 'busboy';
import MetaDataDatabase from '@kominal/connect-models/metadata/metadata.database';
import { ReadStream } from 'fs';
import { connection } from 'mongoose';
const { createModel } = require('mongoose-gridfs');

const router = new Router();

/**
 * Stores files in GridFS storage.
 * @group Protected
 * @security JWT
 * @route POST /gridfs
 * @consumes application/json
 * @produces application/json
 * @param {string} key.body.required - TODO
 * @param {string} data.body.required - TODO
 * @returns {void} 200 - The file was stored in the GridFS storage.
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/gridfs', async (req, res, userId) => {
	const iv = req.query.iv as string;
	verifyParameter(iv);

	const Storeable = createModel({
		modelName: 'Storeable',
		connection,
	});

	const metaData = await MetaDataDatabase.create({
		userId,
		created: new Date(),
		iv: iv.split(','),
	});

	var busboy = new Busboy({ headers: req.headers });
	busboy.on('file', async (fieldname: string, file: NodeJS.ReadableStream, filename: string, encoding: string, mimetype: string) => {
		Storeable.write({ filename: metaData._id }, file, async (error: any, attached: any) => {
			console.log(error, attached);
			await MetaDataDatabase.updateOne({ _id: metaData._id }, { storeableId: attached._id });
		});
	});
	busboy.on('finish', function () {
		res.status(200).send({
			id: metaData._id,
			userId: metaData.get('userId'),
			created: metaData.get('created'),
		});
	});
	req.pipe(busboy);
});

/**
 * Loads the meta data of a stored object
 * @group Protected
 * @security JWT
 * @route POST /gridfs/metadata
 * @consumes application/json
 * @produces application/json
 * @param {string} key.body.required - TODO
 * @param {string} data.body.required - TODO
 * @returns {void} 200 - The file was stored in the GridFS storage.
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/gridfs/metadata', async (req, res) => {
	const id = req.query.id as string;
	verifyParameter(id);
	const metaData = await MetaDataDatabase.findById(id);
	if (!metaData) {
		throw 'error.data.invalid';
	}
	res.status(200).send({
		id: metaData._id,
		userId: metaData.get('userId'),
		created: metaData.get('created'),
		iv: metaData.get('iv'),
	});
});

/**
 * Loads the data of a stored object
 * @group Protected
 * @security JWT
 * @route GET /gridfs
 * @consumes application/json
 * @produces application/json
 * @param {string} key.body.required - TODO
 * @param {string} data.body.required - TODO
 * @returns {void} 200 - The file was stored in the GridFS storage.
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/gridfs', async (req, res) => {
	const id = req.query.id as string;
	verifyParameter(id);
	const metaData = await MetaDataDatabase.findById(id);
	if (!metaData) {
		throw 'error.data.invalid';
	}
	const Storeable = createModel({
		modelName: 'Storeable',
		connection,
	});
	const readStream: ReadStream = Storeable.read({ _id: metaData.get('storeableId') });
	res.setHeader('content-type', 'application/octet-stream');
	readStream.pipe(res);
});

export default router.getExpressRouter();
