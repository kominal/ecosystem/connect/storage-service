import { verifyParameter } from '@kominal/service-util/helper/util';
import StorageDatabase from '@kominal/connect-models/storage/storage.database';
import Router from '@kominal/service-util/helper/router';

const router = new Router();

/**
 * Stores an object in the storage service.
 * @group Protected
 * @security JWT
 * @route POST /
 * @consumes application/json
 * @produces application/json
 * @param {string} data.body.required - The encrypted data
 * @param {string} iv.body.required - The iv used for the encryption
 * @returns {void} 200 - The object was stored
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/', async (req, res, userId) => {
	const { data, iv } = req.body;
	verifyParameter(data, iv);

	const dataObject = await StorageDatabase.create({
		userId,
		created: new Date(),
		data,
		iv,
	});

	res.status(200).send({
		id: dataObject._id,
		userId: dataObject.get('userId'),
		created: dataObject.get('created'),
	});
});

export default router.getExpressRouter();
