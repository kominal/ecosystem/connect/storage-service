import { verifyParameter } from '@kominal/service-util/helper/util';
import StorageDatabase from '@kominal/connect-models/storage/storage.database';
import Router from '@kominal/service-util/helper/router';

const router = new Router();

/**
 * Removes an entry from the storage service.
 * @group Public
 * @route GET /
 * @consumes application/json
 * @produces application/json
 * @param {string} ids.query.required - The ids to delete
 * @returns {void} 200 - The entry was deleted if it previously existed
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsRoot('/remove', async (req, res) => {
	const { ids } = req.body;
	verifyParameter(ids);
	await StorageDatabase.deleteMany({ _id: { $in: ids } });
	res.status(200).send();
});

export default router.getExpressRouter();
